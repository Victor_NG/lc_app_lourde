﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace Application_Lourde_LapinCarotte
{
    class Modele
    {
        #region proprietes
        private MySqlConnection myConnection;   // objet de connexion
        private bool connopen = false;          // test si la connexion est faite
        private bool errgrave = false;          // test si erreur lors de la connexion
        private bool chargement = false;        // test si la requête est faite


        // les DataAdapter et DataTable seront gérés dans des collections avec pour chaque un indice correspondant :
        // indice 0 : récupération des noms des tables
        // indice 1 : Table Constructeur
        // indice 2 : Table Support avec jointure pour récupérer tous les libellés
        // indice 3 : Table Support

        // collection de DataAdapter
        private List<MySqlDataAdapter> dA = new List<MySqlDataAdapter>();

        // collection de DataTable récupérant les données correspond au DA associé
        private List<DataTable> dT = new List<DataTable>();


        private MySqlDataAdapter mySqlDataAdapterImport = new MySqlDataAdapter();
        private DataSet dataSetImport = new DataSet();
        private DataView dv_cours = new DataView(), dv_hebergements = new DataView(), dv_transports = new DataView(), dv_wifi = new DataView(), dv_utilisateurs = new DataView(), dv_etudiant = new DataView(), dv_competence = new DataView(), dv_service = new DataView();

        private char vaction, vtable;


        #endregion

        #region accesseurs

        public MySqlConnection MyConnection { get => myConnection; set => myConnection = value; }
        public bool Connopen { get => connopen; set => connopen = value; }
        public bool Errgrave { get => errgrave; set => errgrave = value; }
        public bool Chargement { get => chargement; set => chargement = value; }
        public List<MySqlDataAdapter> DA { get => dA; set => dA = value; }
        public List<DataTable> DT { get => dT; set => dT = value; }
        public DataView Dv_cours { get => dv_cours; set => dv_cours = value; }
        public DataView Dv_hebergements { get => dv_hebergements; set => dv_hebergements = value; }
        public DataView Dv_transports { get => dv_transports; set => dv_transports = value; }
        public DataView Dv_wifi { get => dv_wifi; set => dv_wifi = value; }
        public DataView Dv_utilisateurs { get => dv_utilisateurs; set => dv_utilisateurs = value; }
        public char Vaction { get => vaction; set => vaction = value; }
        public char Vtable { get => vtable; set => vtable = value; }
        public DataView Dv_etudiant { get => dv_etudiant; set => dv_etudiant = value; }
        public DataView Dv_competence { get => dv_competence; set => dv_competence = value; }
        public DataView Dv_service { get => dv_service; set => dv_service = value; }
        public MySqlDataAdapter MySqlDataAdapterImport { get => mySqlDataAdapterImport; set => mySqlDataAdapterImport = value; }
        public DataSet DataSetImport { get => dataSetImport; set => dataSetImport = value; }


        #endregion

        /// <summary>
        /// Modele() : constructeur permettant l'ajout des DataAdpater et DataTable nécessaires 
        /// indice 0 : récupération des noms des tables
        /// indice 1 : Table Cours
        /// indice 2 : Table Hebergements
        /// indice 3 : Table Transports
        /// indice 4 : Table Wifi
        /// indice 5 : Table Utilisateurs
        /// indice 6 : Table Etudiants
        /// indice 7 : Table Competences
        /// indice 8 : Table Services
        /// </summary>

        public Modele()
        {

            for (int i = 0; i < 9; i++)
            {
                dA.Add(new MySqlDataAdapter());
                dT.Add(new DataTable());
            }
        }

        /// <summary>
        /// méthode seconnecter permettant la connexion à la BD : lapin_carotte
        /// </summary>
        public void seconnecter()
        {

            string myConnectionString = "Database=lc_ppe4;Data Source=192.168.209.5;port=3308;User Id=invite;Password=u3jepk;";
            //string myConnectionString = "Database=lc_ppe4;Data Source=192.168.209.5;User Id=invite;Password=u3jepk;";
            myConnection = new MySqlConnection(myConnectionString);
            try // tentative 
            {
                myConnection.Open();
                connopen = true;
            }
            catch (Exception err)// gestion des erreurs
            {
                MessageBox.Show("Erreur ouverture BD LC : " + err, "PBS connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                connopen = false; errgrave = true;
            }
        }

        public string Connection()
        {
            string myConnectionString = "Database=lc_ppe4;Data Source=192.168.209.5;port=3308;User Id=invite;Password=u3jepk;";
            //string myConnectionString = "Database=lc_ppe4;Data Source=192.168.209.5;User Id=invite;Password=u3jepk;";
            return myConnectionString;
        }


        /// <summary>
        /// méthode sedeconnecter pour se déconnecter à la BD
        /// </summary>
        public void sedeconnecter()
        {
            if (!connopen)
                return;
            try
            {
                myConnection.Close();
                myConnection.Dispose();
                connopen = false;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur fermeture BD LC: " + err, "PBS deconnection", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        public void connexionApp()
        {
            try
            {
                seconnecter();
                string sql = "SELECT LOGIN, MDP FROM admin WHERE LOGIN='" + controleur.recupId() + "'";
                MySqlCommand cmd = new MySqlCommand(sql, myConnection);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    string id = Convert.ToString(rdr[0]);
                    string mdp = Convert.ToString(rdr[1]);
                    if (mdp == controleur.recupMdp())
                    {
                        Form_Accueil FA = new Form_Accueil();
                        FA.Show();
                    }
                    else
                    {
                        MessageBox.Show("Mauvais identifiant ou mot de passe");
                    }
                }
                rdr.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Problème de connexion : " + err, "Erreur requêtage BDD", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
            myConnection.Close();
        }

        /// <summary>
        /// méthode générique privée pour charger le résultat d'une requête dans un dataTable via un dataAdapter
        /// Méthode appelée par charger_donnees(string table)
        /// </summary>
        /// <param name="requete">requete à charger</param>
        /// <param name="DT">dataTable</param>
        /// <param name="DA">dataAdapter</param>
        private void charger(string requete, DataTable DT, MySqlDataAdapter DA)
        {
            DA.SelectCommand = new MySqlCommand(requete, myConnection);

            // pour spécifier les instructions de mise à jour (insert, delete, update)
            MySqlCommandBuilder CB1 = new MySqlCommandBuilder(DA);
            try
            {
                DT.Clear();
                DA.Fill(DT);
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataTable : " + err, "PBS table", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        /// <summary>
        /// charge dans un DT les données de la table passée en paramètre
        /// </summary>
        /// <param name="table">nom de la table à requêter</param>
        public void charger_donnees(string table)
        {
            chargement = false;
            if (!connopen) return;		// pour vérifier que la BD est bien ouverte

            if (table == "toutes")
            {
                charger("show tables;", dT[0], dA[0]);
            }
            if (table == "cours")
            {
                charger("select * from cours ;", dT[1], dA[1]);
            }
            if (table == "hebergement")
            {
                charger("select * from hebergement;", dT[2], dA[2]);
            }
            if (table == "transport")
            {
                charger("select * from transport;", dT[3], dA[3]);
            }
            if (table == "wifi")
            {
                charger("select * from wifi;", dT[4], dA[4]);
            }
            if (table == "utilisateur")
            {
                charger("select * from initiateur;", dT[5], dA[5]);
            }
            if (table == "etudiant")
            {
                charger("select * from etudiant;", dT[6], dA[6]);
            }
            if (table == "competence")
            {
                charger("select * from competence;", dT[7], dA[7]);
            }
            if (table == "service")
            {
                charger("select * from service;", dT[8], dA[8]);
            }

        }

        public void charger_cours()
        {
            DA[1].SelectCommand = new MySqlCommand("select * from cours;", myConnection);
            // pour spécifier les instructions de mise à jour (insert, delete, update)
            MySqlCommandBuilder CB2 = new MySqlCommandBuilder(DA[1]);
            try
            {
                DT[1].Clear();
                DA[1].Fill(DT[1]);
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataTable cours: " + err, "PBS table type",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        public void charger_etudiant()
        {
            DA[6].SelectCommand = new MySqlCommand("select * from etudiant;", myConnection);
            // pour spécifier les instructions de mise à jour (insert, delete, update)
            MySqlCommandBuilder CB2 = new MySqlCommandBuilder(DA[6]);
            try
            {
                DT[6].Clear();
                DA[6].Fill(DT[6]);
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataTable etudiant: " + err, "PBS table type",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        public void charger_competence()
        {
            DA[7].SelectCommand = new MySqlCommand("select * from competences;", myConnection);
            // pour spécifier les instructions de mise à jour (insert, delete, update)
            MySqlCommandBuilder CB2 = new MySqlCommandBuilder(DA[7]);
            try
            {
                DT[7].Clear();
                DA[7].Fill(DT[7]);
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataTable composant: " + err, "PBS table type",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        public void charger_service()
        {
            DA[7].SelectCommand = new MySqlCommand("select * from service;", myConnection);
            // pour spécifier les instructions de mise à jour (insert, delete, update)
            MySqlCommandBuilder CB2 = new MySqlCommandBuilder(DA[8]);
            try
            {
                DT[8].Clear();
                DA[8].Fill(DT[8]);
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataTable composant: " + err, "PBS table type",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        public void charger_Etudiant_Combo(ComboBox CB, int index)
        {

            try
            {
                // comboBox à charger avec les équipes

                MySqlCommand cmd = new MySqlCommand("SELECT NOME, PRENOME FROM etudiant", myConnection);
                MySqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    CB.Items.Add(rdr.GetString(0));
                    string str = rdr.GetString(0);
                }
                rdr.Close();

                if (index > -1)
                {
                    CB.SelectedIndex = index;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Problème de connexion : " + err, "Erreur requêtage BDD", MessageBoxButtons.OK, MessageBoxIcon.Error);
                controleur.Vmodele.Errgrave = true;
            }
        }

        public void charger_Competences_Combo(ComboBox CB, int index)
        {

            try
            {
                // comboBox à charger avec les équipes

                MySqlCommand cmd1 = new MySqlCommand("SELECT LIBELLECOMP FROM competences", myConnection);
                MySqlDataReader rdr1 = cmd1.ExecuteReader();
                while (rdr1.Read())
                {
                    CB.Items.Add(rdr1.GetString(0));
                    string str = rdr1.GetString(0);
                }
                rdr1.Close();

                if (index > -1)
                {
                    CB.SelectedIndex = index;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Problème de connexion : " + err, "Erreur requêtage BDD", MessageBoxButtons.OK, MessageBoxIcon.Error);
                controleur.Vmodele.Errgrave = true;
            }
        }



        public void charger_Services_Combo(ComboBox CB, int index)
        {

            try
            {
                // comboBox à charger avec les équipes

                MySqlCommand cmd2 = new MySqlCommand("SELECT IDSERV FROM service ORDER BY IDSERV ASC", myConnection);
                MySqlDataReader rdr2 = cmd2.ExecuteReader();
                while (rdr2.Read())
                {
                    CB.Items.Add(rdr2.GetString(0));
                    string str = rdr2.GetString(0);
                }
                rdr2.Close();

                if (index > -1)
                {
                    CB.SelectedIndex = index;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Problème de connexion : " + err, "Erreur requêtage BDD", MessageBoxButtons.OK, MessageBoxIcon.Error);
                controleur.Vmodele.Errgrave = true;
            }
        }

        public void charger_ServicesCours_Combo(ComboBox CB, int index)
        {

            try
            {
                // comboBox à charger avec les équipes

                MySqlCommand cmd2 = new MySqlCommand("SELECT IDSERV FROM cours ORDER BY IDSERV ASC", myConnection);
                MySqlDataReader rdr2 = cmd2.ExecuteReader();
                while (rdr2.Read())
                {
                    CB.Items.Add(rdr2.GetString(0));
                    string str = rdr2.GetString(0);
                }
                rdr2.Close();

                if (index > -1)
                {
                    CB.SelectedIndex = index;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Problème de connexion : " + err, "Erreur requêtage BDD", MessageBoxButtons.OK, MessageBoxIcon.Error);
                controleur.Vmodele.Errgrave = true;
            }
        }
    }
}
