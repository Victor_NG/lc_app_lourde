﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Application_Lourde_LapinCarotte
{
    public partial class Form_Services : Form
    {

        private BindingSource bS1;
        private MySqlConnection connection = new MySqlConnection(controleur.Vmodele.Connection());
        public Form_Services()
        {
            InitializeComponent();
        }

        private void Button_Services_Cours_Click(object sender, EventArgs e)
        {
            string message = "Avez vous déjà crée un service?";
            string caption = "Vous devez d'abord créer un service";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;

            DialogResult result;

           
            result = MessageBox.Show(message, caption, buttons);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                controleur.crud_Cours('c', -1, -1);
                // mise à jour du dataGridView en affichage                 
                bS1.MoveLast();
                bS1.MoveFirst();
                controleur.Vmodele.charger_donnees("cours");
            }
            else if ( result == System.Windows.Forms.DialogResult.No)
            {
                controleur.crud_Services('c', -1);
                // mise à jour du dataGridView en affichage                 
                bS1.MoveLast();
                bS1.MoveFirst();
                controleur.Vmodele.charger_donnees("services");
            }
            
        }

        private void Button_Services_Heberg_Click(object sender, EventArgs e)
        {
            Form_Hebergements FH = new Form_Hebergements();
            FH.Show();
        }

        private void Button_Services_Transport_Click(object sender, EventArgs e)
        {
            Form_Transports FT = new Form_Transports();
            FT.Show();
        }

        private void Button_Services_Wifi_Click(object sender, EventArgs e)
        {
            Form_WIFI FW = new Form_WIFI();
            FW.Show();
        }

        private void Form_Services_Load(object sender, EventArgs e)
        {
            controleur.init();
            controleur.Vmodele.seconnecter();
            if (controleur.Vmodele.Connopen == false)
            {
                MessageBox.Show("Erreur dans la connexion");
            }
            else
            {
                MessageBox.Show("BD connectée", "Information BD", MessageBoxButtons.OK, MessageBoxIcon.Information);

                controleur.Vmodele.charger_donnees("service");      // chargement des données de la table POINTSRELAIS
                if (controleur.Vmodele.Chargement)
                {
                    // un DT par table
                    bS1 = new BindingSource();

                    bS1.DataSource = controleur.Vmodele.DT[8];
                    dataGridView_Service.DataSource = bS1;
                    dataGridView_Service.Columns[0].HeaderText = "ID SERVICE";
                    dataGridView_Service.Columns[1].HeaderText = "ID ETUDIANT";
                    dataGridView_Service.Columns[2].HeaderText = "PRIX";
                    dataGridView_Service.Columns[3].HeaderText = "DATE HEURE DEBUT";
                    dataGridView_Service.Columns[4].HeaderText = "DATE HEURE FIN";
                    dataGridView_Service.Columns[5].HeaderText = "ACCEPTER";


                    // mise à jour du dataGridView via le bindingSource rempli par le DataTable
                    dataGridView_Service.Refresh();
                    dataGridView_Service.Visible = true;
                }


            }
        }

        private void button_Ajout_Services_Click(object sender, EventArgs e)
        {
            controleur.crud_Services('c', -1);
            // mise à jour du dataGridView en affichage                 
            bS1.MoveLast();
            bS1.MoveFirst();
            controleur.Vmodele.charger_donnees("service");
        }

        private void button_Modif_Services_Click(object sender, EventArgs e)
        {
            if (dataGridView_Service.SelectedRows.Count == 1)
            {
                // appel de la méthode du controleur en mode update             
                controleur.crud_Services('u', Convert.ToInt32(dataGridView_Service.SelectedRows[0].Index));
                // mise à jour du dataGridView en affichage                 
                bS1.MoveLast();
                bS1.MoveFirst();
                controleur.Vmodele.charger_donnees("service");

            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à modifier");
            }
        }

        private void button_Suppr_Services_Click(object sender, EventArgs e)
        {
            if (dataGridView_Service.SelectedRows.Count == 1)
            {
                // appel de la méthode du controleur en mode update et avec la valeur de Numéro de la musique en clé
                controleur.crud_Services('d', Convert.ToInt32(dataGridView_Service.SelectedRows[0].Index));
                // mise à jour du dataGridView en affichage
                bS1.MoveLast();
                bS1.MoveFirst();
                dataGridView_Service.Refresh();
            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à supprimer");
            }
        }
    }
}