﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


namespace Application_Lourde_LapinCarotte
{
    public partial class Form_Hebergements : Form
    {
        private BindingSource bS1;
        private MySqlConnection connection = new MySqlConnection(controleur.Vmodele.Connection());
        public Form_Hebergements()
        {
            InitializeComponent();
        }

        private void Form_Hebergements_Load(object sender, EventArgs e)
        {
            controleur.init();
            controleur.Vmodele.seconnecter();
            if (controleur.Vmodele.Connopen == false)
            {
                MessageBox.Show("Erreur dans la connexion");
            }
            else
            {
                MessageBox.Show("BD connectée", "Information BD", MessageBoxButtons.OK, MessageBoxIcon.Information);

                controleur.Vmodele.charger_donnees("service");      // chargement des données de la table POINTSRELAIS
                if (controleur.Vmodele.Chargement)
                {
                    // un DT par table
                    bS1 = new BindingSource();

                    bS1.DataSource = controleur.Vmodele.DT[8];
                    dataGridView_Hebergement.DataSource = bS1;
                    dataGridView_Hebergement.Columns[0].HeaderText = "ID SERVICE";
                    dataGridView_Hebergement.Columns[1].HeaderText = "ID ETUDIANT";
                    dataGridView_Hebergement.Columns[2].HeaderText = "PRIX";
                    dataGridView_Hebergement.Columns[3].HeaderText = "DATE HEURE DEBUT";
                    dataGridView_Hebergement.Columns[4].HeaderText = "DATE HEURE FIN";
                    dataGridView_Hebergement.Columns[5].HeaderText = "ACCEPTER";


                    // mise à jour du dataGridView via le bindingSource rempli par le DataTable
                    dataGridView_Hebergement.Refresh();
                    dataGridView_Hebergement.Visible = true;
                }




            }
        }

        private void button_Modif_Heberg_Click(object sender, EventArgs e)
        {
            if (dataGridView_Hebergement.SelectedRows.Count == 1)
            {
                // appel de la méthode du controleur en mode update             
                controleur.crud_Services('u', Convert.ToInt32(dataGridView_Hebergement.SelectedRows[0].Index));
                // mise à jour du dataGridView en affichage                 
                bS1.MoveLast();
                bS1.MoveFirst();
                controleur.Vmodele.charger_donnees("service");

            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à modifier");
            }
        }

        private void button_Supp_Heberg_Click(object sender, EventArgs e)
        {

            if (dataGridView_Hebergement.SelectedRows.Count == 1)
            {
                // appel de la méthode du controleur en mode update et avec la valeur de Numéro de la musique en clé
                controleur.crud_Services('d', Convert.ToInt32(dataGridView_Hebergement.SelectedRows[0].Index));
                // mise à jour du dataGridView en affichage
                bS1.MoveLast();
                bS1.MoveFirst();
                dataGridView_Hebergement.Refresh();
            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à supprimer");
            }
        }

        private void button_Ajout_Heberg_Click(object sender, EventArgs e)
        {
            controleur.crud_Services('c', -1);
            // mise à jour du dataGridView en affichage                 
            bS1.MoveLast();
            bS1.MoveFirst();
            controleur.Vmodele.charger_donnees("service");
        }
    }
    }

