﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Application_Lourde_LapinCarotte
{
    class controleur
    {
        #region propriétés
        private static Modele vmodele;
        public static void init()
        {
            vmodele = new Modele();

        }

        #endregion

        #region accesseurs
        public static Modele Vmodele { get => vmodele; set => vmodele = value; }

        #endregion


        #region methodes
        public static string recupId()
        {
            string id = ((Form_identification)Application.OpenForms["Form_identification"]).TbId.Text;
            return id;
        }

        public static string recupMdp()
        {
            string mdp = ((Form_identification)Application.OpenForms["Form_identification"]).TbMdp.Text;
            return mdp;
        }

        public static void crud_Cours(Char c, int indice, int indice2)
        {
            // Le Char c correspond à l'action : c:create, u :update, d :delete,
            // l'indice est celui de l'enregistrement sélectionné dans le dataGridView, -1 si action d’ajout (c = ‘c’)
            CRUD_Cours CC = new CRUD_Cours(c, indice); // création de la nouvelle forme
            if (c == 'c') // mode ajout donc pas de valeur à passer à la nouvelle forme
            {
                CC.ComboBox1.SelectedIndex = -1;
                vmodele.charger_Services_Combo(CC.ComboBox1, -1);
                CC.ComboBox_Cours_Competence.SelectedIndex = -1;
                vmodele.charger_Competences_Combo(CC.ComboBox_Cours_Competence, -1);
                CC.TextBox_Cours_Lib.Clear();
                CC.TextBox_Cours_Lieu.Clear();

            }
            if (c == 'u' || c == 'd') // mode update ou delete donc on récupère les champs
            {
                vmodele.DT[1].Rows[indice][0].ToString();
                vmodele.charger_ServicesCours_Combo(CC.ComboBox1, indice);
                vmodele.charger_Competences_Combo(CC.ComboBox_Cours_Competence, indice2);
                CC.TextBox_Cours_Lib.Text = vmodele.DT[1].Rows[indice][2].ToString();
                CC.TextBox_Cours_Lieu.Text = vmodele.DT[1].Rows[indice][3].ToString();

            }
            // on affiche la nouvelle form
            if (c == 'u' || c == 'c')
            {
                CC.ShowDialog();
            }

            // si l’utilisateur clique sur OK
            if (CC.DialogResult == DialogResult.OK)
            {
                if (c == 'c') // mode ajout à compléter
                {
                    if (CC.ComboBox1.SelectedIndex != -1 && CC.ComboBox_Cours_Competence.SelectedIndex != -1)
                    {
                        

                        //string Nom_Competence = CC.ComboBox_Cours_Competence.SelectedValue.ToString();
                        int num_competence = Convert.ToInt16(CC.ComboBox_Cours_Competence.SelectedIndex + 1);
                        int ID = Convert.ToInt16(CC.ComboBox1.SelectedItem.ToString());

                        DataRow NewRow_Cours = vmodele.DT[1].NewRow();
                        NewRow_Cours["IDSERV"] = ID;
                        NewRow_Cours["IDCOMP"] = num_competence;
                        NewRow_Cours["LIBELLEC"] = CC.TextBox_Cours_Lib.Text;
                        NewRow_Cours["LIEUC"] = CC.TextBox_Cours_Lieu.Text;



                        vmodele.DT[1].Rows.Add(NewRow_Cours);
                        vmodele.DA[1].Update(vmodele.DT[1]);

                        CC.DataGridView_Cours.Refresh();


                    }
                }
                if (c == 'u') // mode modification
                {
                    if(indice == 0)
                    {
                        indice++;
                    }
                    vmodele.DT[1].Rows[indice]["IDSERV"] = CC.ComboBox1.SelectedIndex +1;
                    vmodele.DT[1].Rows[indice]["IDCOMP"] = CC.ComboBox_Cours_Competence.SelectedIndex +1;
                    vmodele.DT[1].Rows[indice]["LIBELLEC"] = CC.TextBox_Cours_Lib.Text;
                    vmodele.DT[1].Rows[indice]["LIEUC"] = CC.TextBox_Cours_Lieu.Text;
                    vmodele.DA[1].Update(vmodele.DT[1]);

                }
                MessageBox.Show("OK : données enregistrées");
                CC.Dispose(); // on ferme la formCRUD

                if (c == 'd') // mode suppression
                {
                    vmodele.DT[1].Rows[indice].Delete();
                }
            }
            else // cas annuler
            {
                MessageBox.Show("Annulation : aucune donnée enregistrée");
                CC.Dispose();
            }
        }

        public static void crud_Services(Char c, int indice)
        {
            // Le Char c correspond à l'action : c:create, u :update, d :delete,
            // l'indice est celui de l'enregistrement sélectionné dans le dataGridView, -1 si action d’ajout (c = ‘c’)
            CRUD_Service CS = new CRUD_Service(c, indice); // création de la nouvelle forme
            if (c == 'c') // mode ajout donc pas de valeur à passer à la nouvelle forme
            {
                
                CS.ComboBox_S_Etudiant.SelectedIndex = -1;
                CS.TextBox_S_Prix.Clear();
                vmodele.charger_Etudiant_Combo(CS.ComboBox_S_Etudiant, -1);
                CS.MaskedTextBox_HeureD_S.Clear();
                CS.MaskedTextBox_HeureF_S.Clear();
                CS.Bouton_Acccepter_S1.Checked = false;

            }
            if (c == 'u' || c == 'd') // mode update ou delete donc on récupère les champs
            {

                vmodele.DT[8].Rows[indice][0].ToString();
                vmodele.charger_Etudiant_Combo(CS.ComboBox_S_Etudiant, indice);
                CS.ComboBox_S_Etudiant.Enabled = false;
                CS.TextBox_S_Prix.Text = vmodele.DT[8].Rows[indice][2].ToString();
                CS.MaskedTextBox_HeureD_S.Text = vmodele.DT[8].Rows[indice][3].ToString();
                CS.MaskedTextBox_HeureF_S.Text = vmodele.DT[8].Rows[indice][4].ToString();



            }
            // on affiche la nouvelle form
            if (c == 'u' || c == 'c')
            {
                CS.ShowDialog();
            }

            // si l’utilisateur clique sur OK
            if (CS.DialogResult == DialogResult.OK)
            {
                if (c == 'c') // mode ajout à compléter
                {
                    if (CS.ComboBox_S_Etudiant.SelectedIndex != -1)
                    {




                        DataRow NewRow_Service = vmodele.DT[8].NewRow();
                        NewRow_Service["IDE"] = CS.ComboBox_S_Etudiant.SelectedIndex+1;
                        NewRow_Service["PRIX"] = CS.TextBox_S_Prix.Text;
                        NewRow_Service["DATEHEUREDEB"] = CS.MaskedTextBox_HeureD_S.Text;
                        NewRow_Service["DATEHEUREFIN"] = CS.MaskedTextBox_HeureF_S.Text;

                        if (CS.Bouton_Acccepter_S1.Checked == true)
                        {
                            NewRow_Service["ACCEPTER"] = true;
                        }
                        else
                        {
                            NewRow_Service["ACCEPTER"] = false;
                        }


                        vmodele.DT[8].Rows.Add(NewRow_Service);
                        vmodele.DA[8].Update(vmodele.DT[8]);

                    }
                }
                if (c == 'u') // mode modification
                {
                    vmodele.DT[8].Rows[indice]["IDE"] = CS.ComboBox_S_Etudiant.SelectedIndex +1;
                    vmodele.DT[8].Rows[indice]["PRIX"] = CS.TextBox_S_Prix.Text;
                    vmodele.DT[8].Rows[indice]["DATEHEUREDEB"] = CS.MaskedTextBox_HeureD_S.Text;
                    vmodele.DT[8].Rows[indice]["DATEHEUREFIN"] = CS.MaskedTextBox_HeureF_S.Text;

                    if (CS.Bouton_Acccepter_S1.Checked == true)
                    {
                        vmodele.DT[8].Rows[indice]["ACCEPTER"] = true;
                    }
                    else
                    {
                        vmodele.DT[8].Rows[indice]["ACCEPTER"] = false;
                    }



                    vmodele.DA[8].Update(vmodele.DT[8]);
                }
                MessageBox.Show("OK : données enregistrées");
                CS.Dispose(); // on ferme la formCRUD


            }
            else // cas annuler

            {
                if (c == 'd') // mode suppression
                {
                    vmodele.DT[8].Rows[indice].Delete();
                    vmodele.DA[8].Update(vmodele.DT[8]);
                    MessageBox.Show("OK : données enregistrées");
                    CS.Dispose(); // on ferme la formCRUD

                }
                else
                {
                    MessageBox.Show("Annulation : aucune donnée enregistrée");
                    CS.Dispose();
                }

            }
        }

    }
    #endregion
}
