﻿namespace Application_Lourde_LapinCarotte
{
    partial class Form_Hebergements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Supp_Heberg = new System.Windows.Forms.Button();
            this.button_Modif_Heberg = new System.Windows.Forms.Button();
            this.button_Ajout_Heberg = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_Hebergement = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Hebergement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Supp_Heberg
            // 
            this.button_Supp_Heberg.Location = new System.Drawing.Point(590, 650);
            this.button_Supp_Heberg.Name = "button_Supp_Heberg";
            this.button_Supp_Heberg.Size = new System.Drawing.Size(92, 32);
            this.button_Supp_Heberg.TabIndex = 18;
            this.button_Supp_Heberg.Text = "Supprimer";
            this.button_Supp_Heberg.UseVisualStyleBackColor = true;
            this.button_Supp_Heberg.Click += new System.EventHandler(this.button_Supp_Heberg_Click);
            // 
            // button_Modif_Heberg
            // 
            this.button_Modif_Heberg.Location = new System.Drawing.Point(385, 650);
            this.button_Modif_Heberg.Name = "button_Modif_Heberg";
            this.button_Modif_Heberg.Size = new System.Drawing.Size(92, 32);
            this.button_Modif_Heberg.TabIndex = 17;
            this.button_Modif_Heberg.Text = "Modifier";
            this.button_Modif_Heberg.UseVisualStyleBackColor = true;
            this.button_Modif_Heberg.Click += new System.EventHandler(this.button_Modif_Heberg_Click);
            // 
            // button_Ajout_Heberg
            // 
            this.button_Ajout_Heberg.Location = new System.Drawing.Point(188, 650);
            this.button_Ajout_Heberg.Name = "button_Ajout_Heberg";
            this.button_Ajout_Heberg.Size = new System.Drawing.Size(92, 32);
            this.button_Ajout_Heberg.TabIndex = 16;
            this.button_Ajout_Heberg.Text = "Ajouter";
            this.button_Ajout_Heberg.UseVisualStyleBackColor = true;
            this.button_Ajout_Heberg.Click += new System.EventHandler(this.button_Ajout_Heberg_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(345, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(328, 29);
            this.label1.TabIndex = 14;
            this.label1.Text = "Gestion des hébergements";
            // 
            // dataGridView_Hebergement
            // 
            this.dataGridView_Hebergement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Hebergement.Location = new System.Drawing.Point(116, 117);
            this.dataGridView_Hebergement.Name = "dataGridView_Hebergement";
            this.dataGridView_Hebergement.Size = new System.Drawing.Size(654, 510);
            this.dataGridView_Hebergement.TabIndex = 13;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // Form_Hebergements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 695);
            this.Controls.Add(this.button_Supp_Heberg);
            this.Controls.Add(this.button_Modif_Heberg);
            this.Controls.Add(this.button_Ajout_Heberg);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView_Hebergement);
            this.Name = "Form_Hebergements";
            this.Text = "Form_Hebergements";
            this.Load += new System.EventHandler(this.Form_Hebergements_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Hebergement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Supp_Heberg;
        private System.Windows.Forms.Button button_Modif_Heberg;
        private System.Windows.Forms.Button button_Ajout_Heberg;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_Hebergement;
    }
}