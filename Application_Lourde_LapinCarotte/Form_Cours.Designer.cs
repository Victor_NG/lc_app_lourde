﻿namespace Application_Lourde_LapinCarotte
{
    partial class Form_Cours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button_Modif_Cours = new System.Windows.Forms.Button();
            this.button_Suppr_Cours = new System.Windows.Forms.Button();
            this.button_Ajout_Cours = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_Cours = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Cours)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(599, 121);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(143, 21);
            this.comboBox1.TabIndex = 21;
            this.comboBox1.SelectionChangeCommitted += new System.EventHandler(this.ComboBox1_SelectionChangeCommitted);
            // 
            // button_Modif_Cours
            // 
            this.button_Modif_Cours.Location = new System.Drawing.Point(641, 677);
            this.button_Modif_Cours.Name = "button_Modif_Cours";
            this.button_Modif_Cours.Size = new System.Drawing.Size(80, 33);
            this.button_Modif_Cours.TabIndex = 20;
            this.button_Modif_Cours.Text = "Modifier";
            this.button_Modif_Cours.UseVisualStyleBackColor = true;
            this.button_Modif_Cours.Click += new System.EventHandler(this.Button_Modif_Cours_Click);
            // 
            // button_Suppr_Cours
            // 
            this.button_Suppr_Cours.Location = new System.Drawing.Point(786, 677);
            this.button_Suppr_Cours.Name = "button_Suppr_Cours";
            this.button_Suppr_Cours.Size = new System.Drawing.Size(80, 33);
            this.button_Suppr_Cours.TabIndex = 19;
            this.button_Suppr_Cours.Text = "Supprimer";
            this.button_Suppr_Cours.UseVisualStyleBackColor = true;
            this.button_Suppr_Cours.Click += new System.EventHandler(this.Button_Suppr_Cours_Click);
            // 
            // button_Ajout_Cours
            // 
            this.button_Ajout_Cours.Location = new System.Drawing.Point(487, 677);
            this.button_Ajout_Cours.Name = "button_Ajout_Cours";
            this.button_Ajout_Cours.Size = new System.Drawing.Size(80, 33);
            this.button_Ajout_Cours.TabIndex = 18;
            this.button_Ajout_Cours.Text = "Ajouter";
            this.button_Ajout_Cours.UseVisualStyleBackColor = true;
            this.button_Ajout_Cours.Click += new System.EventHandler(this.Button_Ajout_Cours_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(565, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 29);
            this.label1.TabIndex = 16;
            this.label1.Text = "Gestion des cours";
            // 
            // dataGridView_Cours
            // 
            this.dataGridView_Cours.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Cours.Location = new System.Drawing.Point(103, 169);
            this.dataGridView_Cours.Name = "dataGridView_Cours";
            this.dataGridView_Cours.Size = new System.Drawing.Size(1207, 490);
            this.dataGridView_Cours.TabIndex = 15;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // Form_Cours
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 722);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button_Modif_Cours);
            this.Controls.Add(this.button_Suppr_Cours);
            this.Controls.Add(this.button_Ajout_Cours);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView_Cours);
            this.Name = "Form_Cours";
            this.Text = "Form_Cours";
            this.Load += new System.EventHandler(this.Form_Cours_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Cours)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button_Modif_Cours;
        private System.Windows.Forms.Button button_Suppr_Cours;
        private System.Windows.Forms.Button button_Ajout_Cours;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_Cours;
    }
}