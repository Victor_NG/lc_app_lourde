﻿namespace Application_Lourde_LapinCarotte
{
    partial class CRUD_Hebergements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.maskedTextBox_HeureD = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_Prix = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_Etudiants = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_Annuler_Heberg = new System.Windows.Forms.Button();
            this.button_Valider_Heberg = new System.Windows.Forms.Button();
            this.textBox_Adr = new System.Windows.Forms.TextBox();
            this.textBox_Ville = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(243, 314);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(46, 17);
            this.checkBox2.TabIndex = 41;
            this.checkBox2.Text = "Non";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(195, 314);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(42, 17);
            this.checkBox1.TabIndex = 40;
            this.checkBox1.Text = "Oui";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(127, 313);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 17);
            this.label8.TabIndex = 39;
            this.label8.Text = "Lit fait : ";
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(195, 228);
            this.maskedTextBox1.Mask = "00/00/0000 00:00";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBox1.TabIndex = 38;
            this.maskedTextBox1.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(76, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 17);
            this.label7.TabIndex = 37;
            this.label7.Text = "Date heure fin : ";
            // 
            // maskedTextBox_HeureD
            // 
            this.maskedTextBox_HeureD.Location = new System.Drawing.Point(195, 194);
            this.maskedTextBox_HeureD.Mask = "00/00/0000 00:00";
            this.maskedTextBox_HeureD.Name = "maskedTextBox_HeureD";
            this.maskedTextBox_HeureD.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBox_HeureD.TabIndex = 36;
            this.maskedTextBox_HeureD.ValidatingType = typeof(System.DateTime);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(58, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 17);
            this.label6.TabIndex = 35;
            this.label6.Text = "Date heure début : ";
            // 
            // textBox_Prix
            // 
            this.textBox_Prix.Location = new System.Drawing.Point(195, 168);
            this.textBox_Prix.Name = "textBox_Prix";
            this.textBox_Prix.Size = new System.Drawing.Size(225, 20);
            this.textBox_Prix.TabIndex = 34;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(146, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 33;
            this.label5.Text = "Prix : ";
            // 
            // comboBox_Etudiants
            // 
            this.comboBox_Etudiants.FormattingEnabled = true;
            this.comboBox_Etudiants.Location = new System.Drawing.Point(195, 141);
            this.comboBox_Etudiants.Name = "comboBox_Etudiants";
            this.comboBox_Etudiants.Size = new System.Drawing.Size(225, 21);
            this.comboBox_Etudiants.TabIndex = 32;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(117, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 31;
            this.label4.Text = "Etudiant : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Agency FB", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(216, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 31);
            this.label3.TabIndex = 30;
            this.label3.Text = "Hébergements";
            // 
            // button_Annuler_Heberg
            // 
            this.button_Annuler_Heberg.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_Annuler_Heberg.Location = new System.Drawing.Point(279, 372);
            this.button_Annuler_Heberg.Name = "button_Annuler_Heberg";
            this.button_Annuler_Heberg.Size = new System.Drawing.Size(93, 47);
            this.button_Annuler_Heberg.TabIndex = 29;
            this.button_Annuler_Heberg.Text = "Annuler";
            this.button_Annuler_Heberg.UseVisualStyleBackColor = true;
            // 
            // button_Valider_Heberg
            // 
            this.button_Valider_Heberg.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_Valider_Heberg.Location = new System.Drawing.Point(143, 372);
            this.button_Valider_Heberg.Name = "button_Valider_Heberg";
            this.button_Valider_Heberg.Size = new System.Drawing.Size(93, 47);
            this.button_Valider_Heberg.TabIndex = 28;
            this.button_Valider_Heberg.Text = "Valider";
            this.button_Valider_Heberg.UseVisualStyleBackColor = true;
            // 
            // textBox_Adr
            // 
            this.textBox_Adr.Location = new System.Drawing.Point(195, 283);
            this.textBox_Adr.Name = "textBox_Adr";
            this.textBox_Adr.Size = new System.Drawing.Size(225, 20);
            this.textBox_Adr.TabIndex = 27;
            // 
            // textBox_Ville
            // 
            this.textBox_Ville.Location = new System.Drawing.Point(195, 257);
            this.textBox_Ville.Name = "textBox_Ville";
            this.textBox_Ville.Size = new System.Drawing.Size(225, 20);
            this.textBox_Ville.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(114, 286);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 17);
            this.label2.TabIndex = 25;
            this.label2.Text = "Adresse : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(140, 257);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 24;
            this.label1.Text = "Ville : ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // CRUD_Hebergements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 457);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.maskedTextBox_HeureD);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox_Prix);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox_Etudiants);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_Annuler_Heberg);
            this.Controls.Add(this.button_Valider_Heberg);
            this.Controls.Add(this.textBox_Adr);
            this.Controls.Add(this.textBox_Ville);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "CRUD_Hebergements";
            this.Text = "CRUD_Hebergements";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_HeureD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_Prix;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_Etudiants;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_Annuler_Heberg;
        private System.Windows.Forms.Button button_Valider_Heberg;
        private System.Windows.Forms.TextBox textBox_Adr;
        private System.Windows.Forms.TextBox textBox_Ville;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}