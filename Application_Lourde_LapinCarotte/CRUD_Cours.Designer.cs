﻿using System.Windows.Forms;

namespace Application_Lourde_LapinCarotte
{
    partial class CRUD_Cours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_Cours_Lieu = new System.Windows.Forms.TextBox();
            this.comboBox_Cours_Compétence = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_Annuler_Cours = new System.Windows.Forms.Button();
            this.button_Valider_Cours = new System.Windows.Forms.Button();
            this.textBox_Cours_Lib = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.dataGridView_Cours = new System.Windows.Forms.DataGridView();
            this.button_Modif_WiFi = new System.Windows.Forms.Button();
            this.button_Suppr_WiFi = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Cours)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_Cours_Lieu
            // 
            this.textBox_Cours_Lieu.Location = new System.Drawing.Point(208, 254);
            this.textBox_Cours_Lieu.Name = "textBox_Cours_Lieu";
            this.textBox_Cours_Lieu.Size = new System.Drawing.Size(225, 20);
            this.textBox_Cours_Lieu.TabIndex = 61;
            // 
            // comboBox_Cours_Compétence
            // 
            this.comboBox_Cours_Compétence.FormattingEnabled = true;
            this.comboBox_Cours_Compétence.Location = new System.Drawing.Point(208, 197);
            this.comboBox_Cours_Compétence.Name = "comboBox_Cours_Compétence";
            this.comboBox_Cours_Compétence.Size = new System.Drawing.Size(225, 21);
            this.comboBox_Cours_Compétence.TabIndex = 60;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(153, 255);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 17);
            this.label8.TabIndex = 59;
            this.label8.Text = "Lieu : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(250, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 29);
            this.label3.TabIndex = 50;
            this.label3.Text = "Cours";
            // 
            // button_Annuler_Cours
            // 
            this.button_Annuler_Cours.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_Annuler_Cours.Location = new System.Drawing.Point(292, 288);
            this.button_Annuler_Cours.Name = "button_Annuler_Cours";
            this.button_Annuler_Cours.Size = new System.Drawing.Size(93, 47);
            this.button_Annuler_Cours.TabIndex = 49;
            this.button_Annuler_Cours.Text = "Annuler";
            this.button_Annuler_Cours.UseVisualStyleBackColor = true;
            // 
            // button_Valider_Cours
            // 
            this.button_Valider_Cours.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_Valider_Cours.Location = new System.Drawing.Point(156, 288);
            this.button_Valider_Cours.Name = "button_Valider_Cours";
            this.button_Valider_Cours.Size = new System.Drawing.Size(93, 47);
            this.button_Valider_Cours.TabIndex = 48;
            this.button_Valider_Cours.Text = "Valider";
            this.button_Valider_Cours.UseVisualStyleBackColor = true;
            // 
            // textBox_Cours_Lib
            // 
            this.textBox_Cours_Lib.Location = new System.Drawing.Point(208, 224);
            this.textBox_Cours_Lib.Name = "textBox_Cours_Lib";
            this.textBox_Cours_Lib.Size = new System.Drawing.Size(225, 20);
            this.textBox_Cours_Lib.TabIndex = 47;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(100, 224);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 17);
            this.label2.TabIndex = 46;
            this.label2.Text = "Libellé Cours : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(103, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 17);
            this.label1.TabIndex = 45;
            this.label1.Text = "Compétence : ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 44;
            this.pictureBox1.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(208, 154);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(225, 21);
            this.comboBox1.TabIndex = 66;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(135, 154);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 17);
            this.label9.TabIndex = 65;
            this.label9.Text = "Service : ";
            // 
            // dataGridView_Cours
            // 
            this.dataGridView_Cours.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Cours.Location = new System.Drawing.Point(503, 43);
            this.dataGridView_Cours.Name = "dataGridView_Cours";
            this.dataGridView_Cours.Size = new System.Drawing.Size(648, 587);
            this.dataGridView_Cours.TabIndex = 67;
            // 
            // button_Modif_WiFi
            // 
            this.button_Modif_WiFi.Location = new System.Drawing.Point(620, 651);
            this.button_Modif_WiFi.Name = "button_Modif_WiFi";
            this.button_Modif_WiFi.Size = new System.Drawing.Size(144, 54);
            this.button_Modif_WiFi.TabIndex = 69;
            this.button_Modif_WiFi.Text = "Modifier";
            this.button_Modif_WiFi.UseVisualStyleBackColor = true;
            this.button_Modif_WiFi.Click += new System.EventHandler(this.button_Modif_WiFi_Click);
            // 
            // button_Suppr_WiFi
            // 
            this.button_Suppr_WiFi.Location = new System.Drawing.Point(863, 651);
            this.button_Suppr_WiFi.Name = "button_Suppr_WiFi";
            this.button_Suppr_WiFi.Size = new System.Drawing.Size(144, 54);
            this.button_Suppr_WiFi.TabIndex = 68;
            this.button_Suppr_WiFi.Text = "Supprimer";
            this.button_Suppr_WiFi.UseVisualStyleBackColor = true;
            this.button_Suppr_WiFi.Click += new System.EventHandler(this.button_Suppr_WiFi_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(135, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(301, 13);
            this.label4.TabIndex = 70;
            this.label4.Text = "Veillez à prendre une compétence posssédée par la personne!";
            // 
            // CRUD_Cours
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1228, 762);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button_Modif_WiFi);
            this.Controls.Add(this.button_Suppr_WiFi);
            this.Controls.Add(this.dataGridView_Cours);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBox_Cours_Lieu);
            this.Controls.Add(this.comboBox_Cours_Compétence);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_Annuler_Cours);
            this.Controls.Add(this.button_Valider_Cours);
            this.Controls.Add(this.textBox_Cours_Lib);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "CRUD_Cours";
            this.Text = "v";
            this.Load += new System.EventHandler(this.CRUD_Cours_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Cours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Cours_Lieu;
        private System.Windows.Forms.ComboBox comboBox_Cours_Compétence;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_Annuler_Cours;
        private System.Windows.Forms.Button button_Valider_Cours;
        private System.Windows.Forms.TextBox textBox_Cours_Lib;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private ComboBox comboBox1;
        private Label label9;
        private DataGridView dataGridView_Cours;
        private Button button_Modif_WiFi;
        private Button button_Suppr_WiFi;
        private Label label4;


        #region Accesseur



        public ComboBox ComboBox_Cours_Competence
        {
            get
            {
                return comboBox_Cours_Compétence;
            }

            set
            {
                comboBox_Cours_Compétence = value;
            }
        }

        public TextBox TextBox_Cours_Lib
        {
            get
            {
                return textBox_Cours_Lib;
            }

            set
            {
                textBox_Cours_Lib = value;
            }
        }

        public TextBox TextBox_Cours_Lieu
        {
            get
            {
                return textBox_Cours_Lieu;
            }

            set
            {
                textBox_Cours_Lieu = value;
            }
        }



        public ComboBox ComboBox1 { get => comboBox1; set => comboBox1 = value; }
        public DataGridView DataGridView_Cours { get => dataGridView_Cours; set => dataGridView_Cours = value; }
        #endregion
    }
}