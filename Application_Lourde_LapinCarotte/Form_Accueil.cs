﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application_Lourde_LapinCarotte
{
    public partial class Form_Accueil : Form
    {
        public Form_Accueil()
        {
            InitializeComponent();
        }

        private void Button_GestionUtilsateurs_Click(object sender, EventArgs e)
        {
            Form_Utilisateurs FU = new Form_Utilisateurs();
            FU.Show();

        }

        private void Button_GestionServices_Click(object sender, EventArgs e)
        {
            Form_Services FS = new Form_Services();
            FS.Show();
        }
    }
}
