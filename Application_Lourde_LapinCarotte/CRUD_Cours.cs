﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace Application_Lourde_LapinCarotte
{
    public partial class CRUD_Cours : Form
    {
        private BindingSource bS1;
        private MySqlConnection connection = new MySqlConnection(controleur.Vmodele.Connection());
        public CRUD_Cours(char c, int indice)
        {
            InitializeComponent();

        }

        private void CRUD_Cours_Load(object sender, EventArgs e)
        {
            controleur.init();
            controleur.Vmodele.seconnecter();
            if (controleur.Vmodele.Connopen == false)
            {
                MessageBox.Show("Erreur dans la connexion");
            }
            else
            {


                controleur.Vmodele.charger_donnees("cours");      // chargement des données de la table POINTSRELAIS
                if (controleur.Vmodele.Chargement)
                {
                    // un DT par table
                    bS1 = new BindingSource();

                    bS1.DataSource = controleur.Vmodele.DT[1];
                    dataGridView_Cours.DataSource = bS1;



                    DataGridView_Cours.Columns[0].HeaderText = "ID SERVICE";


                    DataGridView_Cours.Columns[2].HeaderText = "COURS";
                    DataGridView_Cours.Columns[3].HeaderText = "LIEU";
                    DataGridView_Cours.Columns[1].HeaderText = "COMPETENCE";

                    // mise à jour du dataGridView via le bindingSource rempli par le DataTable
                    DataGridView_Cours.Refresh();
                    DataGridView_Cours.Visible = true;
                }


            }
        }

        private void comboBox_Cours_Etudiant_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        private void button_Modif_WiFi_Click(object sender, EventArgs e)
        {
            // vérifie qu’une ligne est bien sélectionnée dans le dataGridView             
            if (DataGridView_Cours.SelectedRows.Count == 1)
            {
                // appel de la méthode du controleur en mode update             
                controleur.crud_Cours('u', Convert.ToInt32(DataGridView_Cours.SelectedRows[0].Index), Convert.ToInt32(DataGridView_Cours.CurrentRow.Cells[1].Value) -1 );
                // mise à jour du dataGridView en affichage                 
                bS1.MoveLast();
                bS1.MoveFirst();
                controleur.Vmodele.charger_donnees("cours");

            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à modifier");
            }
        }

        private void button_Suppr_WiFi_Click(object sender, EventArgs e)
        {

            if (dataGridView_Cours.SelectedRows.Count == 1)
            {
                // appel de la méthode du controleur en mode update et avec la valeur de Numéro de la musique en clé
                controleur.crud_Services('d', Convert.ToInt32(dataGridView_Cours.SelectedRows[0].Index));
                // mise à jour du dataGridView en affichage
                bS1.MoveLast();
                bS1.MoveFirst();
                dataGridView_Cours.Refresh();
            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à supprimer");
            }
        }

        private void Button_Valider_Cours_Click(object sender, EventArgs e)
        {

        }
    }



}