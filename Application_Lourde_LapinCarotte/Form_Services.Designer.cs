﻿namespace Application_Lourde_LapinCarotte
{
    partial class Form_Services
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button_Services_Wifi = new System.Windows.Forms.Button();
            this.button_Services_Transport = new System.Windows.Forms.Button();
            this.button_Services_Heberg = new System.Windows.Forms.Button();
            this.button_Services_Cours = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dataGridView_Service = new System.Windows.Forms.DataGridView();
            this.button_Modif_Services = new System.Windows.Forms.Button();
            this.button_Suppr_Services = new System.Windows.Forms.Button();
            this.button_Ajout_Services = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Service)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(422, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 29);
            this.label1.TabIndex = 14;
            this.label1.Text = "Gestion des services";
            // 
            // button_Services_Wifi
            // 
            this.button_Services_Wifi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Services_Wifi.Location = new System.Drawing.Point(22, 388);
            this.button_Services_Wifi.Name = "button_Services_Wifi";
            this.button_Services_Wifi.Size = new System.Drawing.Size(155, 34);
            this.button_Services_Wifi.TabIndex = 13;
            this.button_Services_Wifi.Text = "WI-FI";
            this.button_Services_Wifi.UseVisualStyleBackColor = true;
            this.button_Services_Wifi.Click += new System.EventHandler(this.Button_Services_Wifi_Click);
            // 
            // button_Services_Transport
            // 
            this.button_Services_Transport.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Services_Transport.Location = new System.Drawing.Point(22, 331);
            this.button_Services_Transport.Name = "button_Services_Transport";
            this.button_Services_Transport.Size = new System.Drawing.Size(155, 34);
            this.button_Services_Transport.TabIndex = 12;
            this.button_Services_Transport.Text = "TRANSPORTS";
            this.button_Services_Transport.UseVisualStyleBackColor = true;
            this.button_Services_Transport.Click += new System.EventHandler(this.Button_Services_Transport_Click);
            // 
            // button_Services_Heberg
            // 
            this.button_Services_Heberg.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Services_Heberg.Location = new System.Drawing.Point(22, 268);
            this.button_Services_Heberg.Name = "button_Services_Heberg";
            this.button_Services_Heberg.Size = new System.Drawing.Size(155, 34);
            this.button_Services_Heberg.TabIndex = 11;
            this.button_Services_Heberg.Text = "HEBERGEMENTS";
            this.button_Services_Heberg.UseVisualStyleBackColor = true;
            this.button_Services_Heberg.Click += new System.EventHandler(this.Button_Services_Heberg_Click);
            // 
            // button_Services_Cours
            // 
            this.button_Services_Cours.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Services_Cours.Location = new System.Drawing.Point(22, 214);
            this.button_Services_Cours.Name = "button_Services_Cours";
            this.button_Services_Cours.Size = new System.Drawing.Size(155, 34);
            this.button_Services_Cours.TabIndex = 10;
            this.button_Services_Cours.Text = "COURS";
            this.button_Services_Cours.UseVisualStyleBackColor = true;
            this.button_Services_Cours.Click += new System.EventHandler(this.Button_Services_Cours_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // dataGridView_Service
            // 
            this.dataGridView_Service.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Service.Location = new System.Drawing.Point(233, 135);
            this.dataGridView_Service.Name = "dataGridView_Service";
            this.dataGridView_Service.Size = new System.Drawing.Size(661, 445);
            this.dataGridView_Service.TabIndex = 16;
            // 
            // button_Modif_Services
            // 
            this.button_Modif_Services.Location = new System.Drawing.Point(534, 603);
            this.button_Modif_Services.Name = "button_Modif_Services";
            this.button_Modif_Services.Size = new System.Drawing.Size(80, 33);
            this.button_Modif_Services.TabIndex = 34;
            this.button_Modif_Services.Text = "Modifier";
            this.button_Modif_Services.UseVisualStyleBackColor = true;
            this.button_Modif_Services.Click += new System.EventHandler(this.button_Modif_Services_Click);
            // 
            // button_Suppr_Services
            // 
            this.button_Suppr_Services.Location = new System.Drawing.Point(679, 603);
            this.button_Suppr_Services.Name = "button_Suppr_Services";
            this.button_Suppr_Services.Size = new System.Drawing.Size(80, 33);
            this.button_Suppr_Services.TabIndex = 33;
            this.button_Suppr_Services.Text = "Supprimer";
            this.button_Suppr_Services.UseVisualStyleBackColor = true;
            this.button_Suppr_Services.Click += new System.EventHandler(this.button_Suppr_Services_Click);
            // 
            // button_Ajout_Services
            // 
            this.button_Ajout_Services.Location = new System.Drawing.Point(380, 603);
            this.button_Ajout_Services.Name = "button_Ajout_Services";
            this.button_Ajout_Services.Size = new System.Drawing.Size(80, 33);
            this.button_Ajout_Services.TabIndex = 32;
            this.button_Ajout_Services.Text = "Ajouter";
            this.button_Ajout_Services.UseVisualStyleBackColor = true;
            this.button_Ajout_Services.Click += new System.EventHandler(this.button_Ajout_Services_Click);
            // 
            // Form_Services
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 648);
            this.Controls.Add(this.button_Modif_Services);
            this.Controls.Add(this.button_Suppr_Services);
            this.Controls.Add(this.button_Ajout_Services);
            this.Controls.Add(this.dataGridView_Service);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Services_Wifi);
            this.Controls.Add(this.button_Services_Transport);
            this.Controls.Add(this.button_Services_Heberg);
            this.Controls.Add(this.button_Services_Cours);
            this.Name = "Form_Services";
            this.Text = "Form_Services";
            this.Load += new System.EventHandler(this.Form_Services_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Service)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_Services_Wifi;
        private System.Windows.Forms.Button button_Services_Transport;
        private System.Windows.Forms.Button button_Services_Heberg;
        private System.Windows.Forms.Button button_Services_Cours;
        private System.Windows.Forms.DataGridView dataGridView_Service;
        private System.Windows.Forms.Button button_Modif_Services;
        private System.Windows.Forms.Button button_Suppr_Services;
        private System.Windows.Forms.Button button_Ajout_Services;
    }
}