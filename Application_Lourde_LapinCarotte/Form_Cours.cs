﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Application_Lourde_LapinCarotte
{
    public partial class Form_Cours : Form
    {
        private BindingSource bS1;
        private MySqlConnection connection = new MySqlConnection(controleur.Vmodele.Connection());

        public Form_Cours()
        {
            InitializeComponent();
        }

        private void Form_Cours_Load(object sender, EventArgs e)
        {
            controleur.init();
            controleur.Vmodele.seconnecter();
            if (controleur.Vmodele.Connopen == false)
            {
                MessageBox.Show("Erreur dans la connexion");
            }
            else
            {
                MessageBox.Show("BD connectée", "Information BD", MessageBoxButtons.OK, MessageBoxIcon.Information);

                controleur.Vmodele.charger_donnees("cours");      // chargement des données de la table POINTSRELAIS
                if (controleur.Vmodele.Chargement)
                {
                    // un DT par table
                    bS1 = new BindingSource();


                    bS1.DataSource = controleur.Vmodele.DT[1];
                    dataGridView_Cours.DataSource = bS1 ;
                    dataGridView_Cours.Columns[0].HeaderText = "ID ETUDIANT";
                    dataGridView_Cours.Columns[1].HeaderText = "ID SERVICE";
                    dataGridView_Cours.Columns[9].HeaderText = "ID COMPETENCE";
                    dataGridView_Cours.Columns[7].HeaderText = "LIBELLE";
                    dataGridView_Cours.Columns[8].HeaderText = "LIEU";
                    dataGridView_Cours.Columns[9].HeaderText = "COMPETENCE";
                    dataGridView_Cours.Columns[2].HeaderText = "PRIX";
                    dataGridView_Cours.Columns[4].HeaderText = "DATEHEUREDEB";
                    dataGridView_Cours.Columns[5].HeaderText = "DATEHEUREFIN";
                    dataGridView_Cours.Columns[6].HeaderText = "ACCEPTER";


                    // mise à jour du dataGridView via le bindingSource rempli par le DataTable
                    dataGridView_Cours.Refresh();
                    dataGridView_Cours.Visible = true;
                }




            }
        }

        private void Button_Ajout_Cours_Click(object sender, EventArgs e)
        {
            controleur.crud_Cours('c', -1, 0);
            // mise à jour du dataGridView en affichage                 
            bS1.MoveLast();
            bS1.MoveFirst();
            controleur.Vmodele.charger_donnees("cours");
        }

        private void Button_Modif_Cours_Click(object sender, EventArgs e)
        {
            // vérifie qu’une ligne est bien sélectionnée dans le dataGridView             
            if (dataGridView_Cours.SelectedRows.Count == 1)
            {
                // appel de la méthode du controleur en mode update             
                controleur.crud_Cours('u', Convert.ToInt32(dataGridView_Cours.SelectedRows[0].Index), 0);
                // mise à jour du dataGridView en affichage                 
                bS1.MoveLast();
                bS1.MoveFirst();
                controleur.Vmodele.charger_donnees("cours");

            }
            else
            {
                MessageBox.Show("Sélectionner une ligne à modifier");
            }
        }

        private void Button_Suppr_Cours_Click(object sender, EventArgs e)
        {
            int ID = Convert.ToInt16(dataGridView_Cours.CurrentRow.Cells["IDSERV"].Value);
            string query = $"DELETE FROM cours WHERE IDSERV='{ID}'";
            MySqlCommand cmd = new MySqlCommand(query, connection);
            cmd.ExecuteNonQuery();
        }

        private void chargeDonnees()
        {
            // comboBox à charger avec les équipes
            comboBox1.Items.Clear();
            List<KeyValuePair<int, string>> uList = new List<KeyValuePair<int, string>>();
            uList.Add(new KeyValuePair<int, string>(0, "toutes les équipes"));
            comboBox1.Items.Add("toutes les équipes");
            for (int i = 0; i < controleur.Vmodele.Dv_competence.ToTable().Rows.Count; i++)
            {
                uList.Add(new KeyValuePair<int, string>((int)controleur.Vmodele.Dv_competence.ToTable().Rows[i][0], controleur.Vmodele.Dv_competence.ToTable().Rows[i][1].ToString()));
            }
            comboBox1.DataSource = uList;
            comboBox1.ValueMember = "Key";
            comboBox1.DisplayMember = "Value";
            comboBox1.Text = comboBox1.Items[0].ToString();

            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;

            // pour afficher tous les coureurs dans le dataGridView
            bS1.DataSource = controleur.Vmodele.Dv_competence;
            dataGridView_Cours.DataSource = bS1;


            // amélioration de l'affichage du dataGridView
            dataGridView_Cours.Columns[0].HeaderText = "ID SERVICE";
            dataGridView_Cours.Columns[1].HeaderText = "ID COMPETENCE";
            dataGridView_Cours.Columns[2].HeaderText = "LIBELLE";
            dataGridView_Cours.Columns[3].HeaderText = "LIEU";
            dataGridView_Cours.Columns[4].HeaderText = "PRIX";
            dataGridView_Cours.Columns[5].HeaderText = "DATE HEURE DEBUT";
            dataGridView_Cours.Columns[6].HeaderText = "DATE HEURE FIN";
            dataGridView_Cours.Columns[7].HeaderText = "ACCEPTER";


            dataGridView_Cours.Refresh();
        }

        private void changefiltre()
        {
            string num = comboBox1.SelectedValue.ToString();

            int n = Convert.ToInt32(num);
            if (n == 0) // cas de "toutes les équipes"
                controleur.Vmodele.Dv_competence.RowFilter = "";
            else
            {
                string Filter = "IdEqu = '" + n + "'";
                controleur.Vmodele.Dv_competence.RowFilter = Filter;
            }
            dataGridView_Cours.Refresh();

        }

        private void ComboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            changefiltre();
        }

        private int RechercheCompetence(int n)
        {
            int index = -1;
            for (int i = 0; i < controleur.Vmodele.Dv_competence.Count; i++)
            {
                if (n == (int)controleur.Vmodele.Dv_competence.ToTable().Rows[i][0])
                {
                    index = i;
                }
            }
            return index;
        }
    }


}
