﻿namespace Application_Lourde_LapinCarotte
{
    partial class Form_Transports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Modif_Transports = new System.Windows.Forms.Button();
            this.button_Suppr_Transports = new System.Windows.Forms.Button();
            this.button_Ajout_Transports = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_Users = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Users)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Modif_Transports
            // 
            this.button_Modif_Transports.Location = new System.Drawing.Point(366, 633);
            this.button_Modif_Transports.Name = "button_Modif_Transports";
            this.button_Modif_Transports.Size = new System.Drawing.Size(80, 33);
            this.button_Modif_Transports.TabIndex = 25;
            this.button_Modif_Transports.Text = "Modifier";
            this.button_Modif_Transports.UseVisualStyleBackColor = true;
            // 
            // button_Suppr_Transports
            // 
            this.button_Suppr_Transports.Location = new System.Drawing.Point(511, 633);
            this.button_Suppr_Transports.Name = "button_Suppr_Transports";
            this.button_Suppr_Transports.Size = new System.Drawing.Size(80, 33);
            this.button_Suppr_Transports.TabIndex = 24;
            this.button_Suppr_Transports.Text = "Supprimer";
            this.button_Suppr_Transports.UseVisualStyleBackColor = true;
            // 
            // button_Ajout_Transports
            // 
            this.button_Ajout_Transports.Location = new System.Drawing.Point(212, 633);
            this.button_Ajout_Transports.Name = "button_Ajout_Transports";
            this.button_Ajout_Transports.Size = new System.Drawing.Size(80, 33);
            this.button_Ajout_Transports.TabIndex = 23;
            this.button_Ajout_Transports.Text = "Ajouter";
            this.button_Ajout_Transports.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Agency FB", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(334, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 31);
            this.label1.TabIndex = 21;
            this.label1.Text = "Gestion des transports";
            // 
            // dataGridView_Users
            // 
            this.dataGridView_Users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Users.Location = new System.Drawing.Point(108, 120);
            this.dataGridView_Users.Name = "dataGridView_Users";
            this.dataGridView_Users.Size = new System.Drawing.Size(671, 490);
            this.dataGridView_Users.TabIndex = 20;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(12, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // Form_Transports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 677);
            this.Controls.Add(this.button_Modif_Transports);
            this.Controls.Add(this.button_Suppr_Transports);
            this.Controls.Add(this.button_Ajout_Transports);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView_Users);
            this.Name = "Form_Transports";
            this.Text = "Form_Transports";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Users)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Modif_Transports;
        private System.Windows.Forms.Button button_Suppr_Transports;
        private System.Windows.Forms.Button button_Ajout_Transports;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_Users;
    }
}