﻿namespace Application_Lourde_LapinCarotte
{
    partial class CRUD_WIFI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.maskedTextBox_HeureD = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_Prix = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_Etudiants = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_Annuler_WIFI = new System.Windows.Forms.Button();
            this.button_Valider_WIFI = new System.Windows.Forms.Button();
            this.textBox_Adr = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(139, 257);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 17);
            this.label8.TabIndex = 53;
            this.label8.Text = "Lieu : ";
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(195, 228);
            this.maskedTextBox1.Mask = "00/00/0000 00:00";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBox1.TabIndex = 52;
            this.maskedTextBox1.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(76, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 17);
            this.label7.TabIndex = 51;
            this.label7.Text = "Date heure fin : ";
            // 
            // maskedTextBox_HeureD
            // 
            this.maskedTextBox_HeureD.Location = new System.Drawing.Point(195, 194);
            this.maskedTextBox_HeureD.Mask = "00/00/0000 00:00";
            this.maskedTextBox_HeureD.Name = "maskedTextBox_HeureD";
            this.maskedTextBox_HeureD.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBox_HeureD.TabIndex = 50;
            this.maskedTextBox_HeureD.ValidatingType = typeof(System.DateTime);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(58, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 17);
            this.label6.TabIndex = 49;
            this.label6.Text = "Date heure début : ";
            // 
            // textBox_Prix
            // 
            this.textBox_Prix.Location = new System.Drawing.Point(195, 168);
            this.textBox_Prix.Name = "textBox_Prix";
            this.textBox_Prix.Size = new System.Drawing.Size(225, 20);
            this.textBox_Prix.TabIndex = 48;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(146, 171);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 47;
            this.label5.Text = "Prix : ";
            // 
            // comboBox_Etudiants
            // 
            this.comboBox_Etudiants.FormattingEnabled = true;
            this.comboBox_Etudiants.Location = new System.Drawing.Point(195, 141);
            this.comboBox_Etudiants.Name = "comboBox_Etudiants";
            this.comboBox_Etudiants.Size = new System.Drawing.Size(225, 21);
            this.comboBox_Etudiants.TabIndex = 46;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(117, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 45;
            this.label4.Text = "Etudiant : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Agency FB", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(216, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 31);
            this.label3.TabIndex = 44;
            this.label3.Text = "Wi-Fi";
            // 
            // button_Annuler_WIFI
            // 
            this.button_Annuler_WIFI.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_Annuler_WIFI.Location = new System.Drawing.Point(276, 320);
            this.button_Annuler_WIFI.Name = "button_Annuler_WIFI";
            this.button_Annuler_WIFI.Size = new System.Drawing.Size(93, 47);
            this.button_Annuler_WIFI.TabIndex = 43;
            this.button_Annuler_WIFI.Text = "Annuler";
            this.button_Annuler_WIFI.UseVisualStyleBackColor = true;
            this.button_Annuler_WIFI.Click += new System.EventHandler(this.Button_Annuler_Click);
            // 
            // button_Valider_WIFI
            // 
            this.button_Valider_WIFI.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_Valider_WIFI.Location = new System.Drawing.Point(142, 320);
            this.button_Valider_WIFI.Name = "button_Valider_WIFI";
            this.button_Valider_WIFI.Size = new System.Drawing.Size(93, 47);
            this.button_Valider_WIFI.TabIndex = 42;
            this.button_Valider_WIFI.Text = "Valider";
            this.button_Valider_WIFI.UseVisualStyleBackColor = true;
            // 
            // textBox_Adr
            // 
            this.textBox_Adr.Location = new System.Drawing.Point(195, 254);
            this.textBox_Adr.Name = "textBox_Adr";
            this.textBox_Adr.Size = new System.Drawing.Size(225, 20);
            this.textBox_Adr.TabIndex = 41;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 40;
            this.pictureBox1.TabStop = false;
            // 
            // CRUD_WIFI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 410);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.maskedTextBox_HeureD);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox_Prix);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox_Etudiants);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_Annuler_WIFI);
            this.Controls.Add(this.button_Valider_WIFI);
            this.Controls.Add(this.textBox_Adr);
            this.Controls.Add(this.pictureBox1);
            this.Name = "CRUD_WIFI";
            this.Text = "CRUD_WIFI";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_HeureD;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_Prix;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_Etudiants;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_Annuler_WIFI;
        private System.Windows.Forms.Button button_Valider_WIFI;
        private System.Windows.Forms.TextBox textBox_Adr;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}