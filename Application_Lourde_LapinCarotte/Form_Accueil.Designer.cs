﻿namespace Application_Lourde_LapinCarotte
{
    partial class Form_Accueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Statistiques = new System.Windows.Forms.Button();
            this.button_GestionServices = new System.Windows.Forms.Button();
            this.button_GestionUtilsateurs = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Statistiques
            // 
            this.button_Statistiques.Location = new System.Drawing.Point(157, 281);
            this.button_Statistiques.Name = "button_Statistiques";
            this.button_Statistiques.Size = new System.Drawing.Size(144, 41);
            this.button_Statistiques.TabIndex = 10;
            this.button_Statistiques.Text = "Statistiques";
            this.button_Statistiques.UseVisualStyleBackColor = true;
            // 
            // button_GestionServices
            // 
            this.button_GestionServices.Location = new System.Drawing.Point(157, 222);
            this.button_GestionServices.Name = "button_GestionServices";
            this.button_GestionServices.Size = new System.Drawing.Size(144, 41);
            this.button_GestionServices.TabIndex = 9;
            this.button_GestionServices.Text = "Gestion Services";
            this.button_GestionServices.UseVisualStyleBackColor = true;
            this.button_GestionServices.Click += new System.EventHandler(this.Button_GestionServices_Click);
            // 
            // button_GestionUtilsateurs
            // 
            this.button_GestionUtilsateurs.Location = new System.Drawing.Point(157, 162);
            this.button_GestionUtilsateurs.Name = "button_GestionUtilsateurs";
            this.button_GestionUtilsateurs.Size = new System.Drawing.Size(144, 41);
            this.button_GestionUtilsateurs.TabIndex = 8;
            this.button_GestionUtilsateurs.Text = "Gestion Utilisateurs";
            this.button_GestionUtilsateurs.UseVisualStyleBackColor = true;
            this.button_GestionUtilsateurs.Click += new System.EventHandler(this.Button_GestionUtilsateurs_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(171, 115);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // Form_Accueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 407);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_Statistiques);
            this.Controls.Add(this.button_GestionServices);
            this.Controls.Add(this.button_GestionUtilsateurs);
            this.Name = "Form_Accueil";
            this.Text = "Form_Utilisateurs";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button_Statistiques;
        private System.Windows.Forms.Button button_GestionServices;
        private System.Windows.Forms.Button button_GestionUtilsateurs;
    }
}