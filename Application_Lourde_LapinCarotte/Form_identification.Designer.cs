﻿using System.Windows.Forms;

namespace Application_Lourde_LapinCarotte
{
    partial class Form_identification
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Identifiant = new System.Windows.Forms.Label();
            this.mdp = new System.Windows.Forms.Label();
            this.tbId = new System.Windows.Forms.TextBox();
            this.tbMdp = new System.Windows.Forms.TextBox();
            this.btValider = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(108, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(203, 196);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Identifiant
            // 
            this.Identifiant.AutoSize = true;
            this.Identifiant.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Identifiant.Location = new System.Drawing.Point(156, 233);
            this.Identifiant.Name = "Identifiant";
            this.Identifiant.Size = new System.Drawing.Size(92, 22);
            this.Identifiant.TabIndex = 1;
            this.Identifiant.Text = "Identifiant";
            // 
            // mdp
            // 
            this.mdp.AutoSize = true;
            this.mdp.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mdp.Location = new System.Drawing.Point(143, 310);
            this.mdp.Name = "mdp";
            this.mdp.Size = new System.Drawing.Size(125, 22);
            this.mdp.TabIndex = 2;
            this.mdp.Text = "Mot de passe";
            // 
            // tbId
            // 
            this.tbId.Location = new System.Drawing.Point(116, 271);
            this.tbId.Name = "tbId";
            this.tbId.Size = new System.Drawing.Size(184, 20);
            this.tbId.TabIndex = 3;
            // 
            // tbMdp
            // 
            this.tbMdp.Location = new System.Drawing.Point(116, 347);
            this.tbMdp.Name = "tbMdp";
            this.tbMdp.PasswordChar = '*';
            this.tbMdp.Size = new System.Drawing.Size(184, 20);
            this.tbMdp.TabIndex = 4;
            // 
            // btValider
            // 
            this.btValider.BackColor = System.Drawing.Color.White;
            this.btValider.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btValider.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btValider.Location = new System.Drawing.Point(160, 409);
            this.btValider.Name = "btValider";
            this.btValider.Size = new System.Drawing.Size(88, 42);
            this.btValider.TabIndex = 5;
            this.btValider.Text = "Valider";
            this.btValider.UseVisualStyleBackColor = false;
            this.btValider.Click += new System.EventHandler(this.BtValider_Click);
            // 
            // Form_identification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(411, 496);
            this.Controls.Add(this.btValider);
            this.Controls.Add(this.tbMdp);
            this.Controls.Add(this.tbId);
            this.Controls.Add(this.mdp);
            this.Controls.Add(this.Identifiant);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form_identification";
            this.Text = "Identification";
            this.Load += new System.EventHandler(this.Form_identification_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label Identifiant;
        private System.Windows.Forms.Label mdp;
        private System.Windows.Forms.TextBox tbId;
        private System.Windows.Forms.TextBox tbMdp;
        private System.Windows.Forms.Button btValider;

        public TextBox TbId { get => tbId; set => tbId = value; }
        public TextBox TbMdp { get => tbMdp; set => tbMdp = value; }
    }
}

