﻿using System.Windows.Forms;

namespace Application_Lourde_LapinCarotte
{
    partial class CRUD_Service
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Bouton_Acccepter_S = new System.Windows.Forms.CheckBox();
            this.comboBox_S_Etudiant = new System.Windows.Forms.ComboBox();
            this.maskedTextBox_HeureF_S = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.maskedTextBox_HeureD_S = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_Annuler_S = new System.Windows.Forms.Button();
            this.button_Valider_S = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox_S_Prix = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Bouton_Acccepter_S
            // 
            this.Bouton_Acccepter_S.AutoSize = true;
            this.Bouton_Acccepter_S.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Bouton_Acccepter_S.Location = new System.Drawing.Point(179, 241);
            this.Bouton_Acccepter_S.Name = "Bouton_Acccepter_S";
            this.Bouton_Acccepter_S.Size = new System.Drawing.Size(81, 20);
            this.Bouton_Acccepter_S.TabIndex = 5;
            this.Bouton_Acccepter_S.Text = "Accepter";
            this.Bouton_Acccepter_S.UseVisualStyleBackColor = true;
            // 
            // comboBox_S_Etudiant
            // 
            this.comboBox_S_Etudiant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_S_Etudiant.FormattingEnabled = true;
            this.comboBox_S_Etudiant.Location = new System.Drawing.Point(156, 118);
            this.comboBox_S_Etudiant.Name = "comboBox_S_Etudiant";
            this.comboBox_S_Etudiant.Size = new System.Drawing.Size(225, 21);
            this.comboBox_S_Etudiant.TabIndex = 1;
            // 
            // maskedTextBox_HeureF_S
            // 
            this.maskedTextBox_HeureF_S.Location = new System.Drawing.Point(156, 205);
            this.maskedTextBox_HeureF_S.Mask = "00/00/0000 00:00";
            this.maskedTextBox_HeureF_S.Name = "maskedTextBox_HeureF_S";
            this.maskedTextBox_HeureF_S.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBox_HeureF_S.TabIndex = 4;
            this.maskedTextBox_HeureF_S.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(40, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 17);
            this.label7.TabIndex = 79;
            this.label7.Text = "Date heure fin : ";
            // 
            // maskedTextBox_HeureD_S
            // 
            this.maskedTextBox_HeureD_S.Location = new System.Drawing.Point(156, 171);
            this.maskedTextBox_HeureD_S.Mask = "00/00/0000 00:00";
            this.maskedTextBox_HeureD_S.Name = "maskedTextBox_HeureD_S";
            this.maskedTextBox_HeureD_S.Size = new System.Drawing.Size(225, 20);
            this.maskedTextBox_HeureD_S.TabIndex = 3;
            this.maskedTextBox_HeureD_S.ValidatingType = typeof(System.DateTime);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(19, 171);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 17);
            this.label6.TabIndex = 77;
            this.label6.Text = "Date heure début : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(107, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 75;
            this.label5.Text = "Prix : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(78, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 17);
            this.label4.TabIndex = 74;
            this.label4.Text = "Etudiant : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(206, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 29);
            this.label3.TabIndex = 73;
            this.label3.Text = "SERVICE";
            // 
            // button_Annuler_S
            // 
            this.button_Annuler_S.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button_Annuler_S.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Annuler_S.Location = new System.Drawing.Point(236, 277);
            this.button_Annuler_S.Name = "button_Annuler_S";
            this.button_Annuler_S.Size = new System.Drawing.Size(93, 47);
            this.button_Annuler_S.TabIndex = 7;
            this.button_Annuler_S.Text = "Annuler";
            this.button_Annuler_S.UseVisualStyleBackColor = true;
            // 
            // button_Valider_S
            // 
            this.button_Valider_S.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button_Valider_S.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Valider_S.Location = new System.Drawing.Point(100, 277);
            this.button_Valider_S.Name = "button_Valider_S";
            this.button_Valider_S.Size = new System.Drawing.Size(93, 47);
            this.button_Valider_S.TabIndex = 6;
            this.button_Valider_S.Text = "Valider";
            this.button_Valider_S.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Application_Lourde_LapinCarotte.Properties.Resources.lapincarotte;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 84);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 67;
            this.pictureBox1.TabStop = false;
            // 
            // textBox_S_Prix
            // 
            this.textBox_S_Prix.Location = new System.Drawing.Point(156, 147);
            this.textBox_S_Prix.Mask = "00.00";
            this.textBox_S_Prix.Name = "textBox_S_Prix";
            this.textBox_S_Prix.Size = new System.Drawing.Size(225, 20);
            this.textBox_S_Prix.TabIndex = 2;
            // 
            // CRUD_Service
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 346);
            this.Controls.Add(this.textBox_S_Prix);
            this.Controls.Add(this.Bouton_Acccepter_S);
            this.Controls.Add(this.comboBox_S_Etudiant);
            this.Controls.Add(this.maskedTextBox_HeureF_S);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.maskedTextBox_HeureD_S);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_Annuler_S);
            this.Controls.Add(this.button_Valider_S);
            this.Controls.Add(this.pictureBox1);
            this.Name = "CRUD_Service";
            this.Text = "CRUD_Service";
            this.Load += new System.EventHandler(this.CRUD_Service_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox Bouton_Acccepter_S;
        private System.Windows.Forms.ComboBox comboBox_S_Etudiant;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_HeureF_S;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.MaskedTextBox maskedTextBox_HeureD_S;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_Annuler_S;
        private System.Windows.Forms.Button button_Valider_S;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MaskedTextBox textBox_S_Prix;

        public ComboBox ComboBox_S_Etudiant { get => comboBox_S_Etudiant; set => comboBox_S_Etudiant = value; }
        public MaskedTextBox MaskedTextBox_HeureF_S { get => maskedTextBox_HeureF_S; set => maskedTextBox_HeureF_S = value; }
        public MaskedTextBox MaskedTextBox_HeureD_S { get => maskedTextBox_HeureD_S; set => maskedTextBox_HeureD_S = value; }

        public CheckBox Bouton_Acccepter_S1 { get => Bouton_Acccepter_S; set => Bouton_Acccepter_S = value; }
        public MaskedTextBox TextBox_S_Prix { get => textBox_S_Prix; set => textBox_S_Prix = value; }
    }
}